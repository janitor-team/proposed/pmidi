%define ver	1.7.1
%define rel	1
%define prefix	/usr

Name:		pmidi
Summary:	A Command Line MIDI Player for ALSA.
BuildRequires: alsa-lib-devel
Version:	%{ver}
Release:	%{rel}
License:	GPL-2
Group:		Applications/Multimedia/Audio
Source: 	%{name}-%{version}.tar.gz
URL:		http://pmidi.sourceforge.net
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{rel}-root

%description
pmidi is a command line midi player for ALSA.  It can play to any MIDI
device that is supported by ALSA.


%prep
%setup -q


%build
autoreconf -i
%configure --with-included-glib
make


%install
[ -d ${RPM_BUILD_ROOT} ] && rm -rf ${RPM_BUILD_ROOT}
make DESTDIR=${RPM_BUILD_ROOT} install


%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT


%files
%defattr (-, root, root)
%{_bindir}/*
%doc AUTHORS
%doc COPYING
%doc ChangeLog
%doc INSTALL
%doc NEWS
%doc README
%doc %{_mandir}/man*/*

%changelog
* Fri Dec 13 2013 sr@parabola.me.uk
- Completely simplify the spec file
- Remove irrelevant info from description
